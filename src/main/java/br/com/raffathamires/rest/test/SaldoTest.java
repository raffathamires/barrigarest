package br.com.raffathamires.rest.test;

import br.com.raffathamires.rest.core.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class SaldoTest extends BaseTest {

    @Test
    public void deveCalcularSaldoDasContas(){
        Integer CONTA_ID = getIDContaPeloNome("Conta para saldo");

        given()
        .when()
                .get("/saldo")
        .then()
                .statusCode(200)
                .body("find{it.conta_id ==" + CONTA_ID + "}.saldo", is("534.00"))
        ;
    }
}
