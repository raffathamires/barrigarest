package br.com.raffathamires.rest.test;

import br.com.raffathamires.rest.core.BaseTest;
import org.junit.Test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class ContasTest extends BaseTest {

    @Test
    public void deveIncluirContaComSucesso(){
        given()
                .body("{\"nome\":\"Conta Inserida\"}")
        .when()
                .post("/contas")
        .then()
                .statusCode(201)
                .body("id", notNullValue())
        ;
    }

    @Test
    public void deveAlterarContaComSucesso(){
        Integer CONTA_ID = getIDContaPeloNome("Conta para alterar");

        given()
                .body("{\"nome\":\"Conta alterada\"}")
                .pathParam("id", CONTA_ID)
        .when()
                .put("/contas/{id}")
        .then()
                .statusCode(200)
                .body("nome", is("Conta alterada"))
        ;
    }

    @Test
    public void naoDeveIncluirContaComNomeRepetido(){
        given()
                .body("{\"nome\":\"Conta mesmo nome\"}")
        .when()
                .post("/contas")
        .then()
                .statusCode(400)
                .body("error", is("Já existe uma conta com esse nome!"))
        ;
    }
}
