package br.com.raffathamires.rest.core;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import br.com.raffathamires.rest.page.Movimentacao;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;

import org.junit.BeforeClass;

import java.util.HashMap;
import java.util.Map;

public class BaseTest implements Constantes{

    @BeforeClass
    public static void setup(){
        baseURI = APP_BASE_URL;
        port = APP_PORT;
        basePath = APP_BASE_PATH;

        RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
        reqBuilder.setContentType(APP_CONTENT_TYPE);
        requestSpecification = reqBuilder.build();

        ResponseSpecBuilder respBuilder = new ResponseSpecBuilder();
        respBuilder.expectResponseTime(lessThan(MAX_TIMEOUT));
        responseSpecification = respBuilder.build();

        enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @BeforeClass
    public static void login(){
        Map<String, String> login = new HashMap<String, String>();
        login.put("email", "raffaela.monteiro@gmail.com");
        login.put("senha", "doddy");

        String TOKEN =
                given()
                        .body(login)
                        .when()
                        .post("/signin")
                        .then()
                        .statusCode(200)
                        .extract().path("token")
                ;
        requestSpecification.header("Authorization", "JWT " + TOKEN); //set do token para todos os testes

        //resetar o banco
        RestAssured.get("/reset").then().statusCode(200);
    }

    public static Integer getIDContaPeloNome(String nome){
        return RestAssured.get("/contas?nome="+nome).then().extract().path("id[0]");
    }

    public static Integer getIDMovimentacaoPelaDescricao(String descricao){
        return RestAssured.get("/transacoes?descricao="+descricao).then().extract().path("id[0]");
    }

    public Movimentacao getMovimentacaoValida(){
        Movimentacao mov = new Movimentacao();
        mov.setConta_id(getIDContaPeloNome("Conta para movimentacoes"));
        mov.setDescricao("Descricao da movimentacao");
        mov.setEnvolvido("Envolvido na movimentacao");
        mov.setTipo("REC");
        mov.setData_transacao(DataUtils.getDataDiferencaDias(DIFERENCA_DIAS_PASSADO));
        mov.setData_pagamento(DataUtils.getDataDiferencaDias(DIFERENCA_DIAS_HOJE));
        mov.setValor(100f);
        mov.setStatus(true);

        return mov;
    }
}
